(* Basic gates and circuits *)

(* The tagless-final style is designed around a compositional mapping of
terms of the embedded DSL to the values in some |repr| domain -- i.e.,
denotational semantics.
*)

(* 
 * Define the syntax of circuits (in the form close to BNF)
 * Read  ``repr'' as a wire



*)

module type SYM = sig
  type repr

  val lit  : bool -> repr
  val neg  : repr -> repr
  val (&&) : repr -> repr -> repr
  val (||) : repr -> repr -> repr
end

(* NNN 
   Module, aka struct: a collection of type declaration and definitions
   Module type, aka signature: a collection of type declarations and types of 
                the defined symbols
   Modules have a lot in common with records or objects (sans declarations),
           or namespaces
   and module types -- with virtual/abstract classes
*)


(* BNF view
The signature |SYM| can be read as the context-free grammar of the language:
|repr| is the start symbol and |lit|, |neg|, etc. are the
terminals. In the conventional CFG notation it would look as:

repr -> "lit" "true" | "lit" "false"
repr -> "neg" repr
repr -> repr "&&" repr
repr -> repr "||" repr

This grammar derives, for example, |lit true && lit false|.
*)

(* OCaml programmer's view *)
(*








*)

(* Example
   NNN module, parameterized module (functor), open, let definition
*)
module Ex1(I:SYM) = struct
  open I
  let wire = lit true && (neg (lit false))
end

(* without parentheses: |lit true && neg @@ lit false| *)

(*






*)

(* A different way of writing Ex1, that may appeal to some people
   It emphasizes that modules are essentially `records'
*)

(* Treat the module SYM as if it were a record
   {lit: bool -> 'a; neg: 'a -> 'a; ...}
*)

type 'a symmod = (module SYM with type repr = 'a)

let ex1 : type a. a symmod -> a = function (module I) -> 
  I.(&&) (I.lit true)  (I.neg (I.lit false))

let ex1 : type a. a symmod -> a = function (module I) ->
  I.(lit true && (neg (lit false)))

let ex1 : type a. a symmod -> a = function (module I) -> let open I in
  lit true && (neg (lit false))
  
(* An alternative
   `Object algebras' (although it was presented at SSGIP in 2010,
   two years before object algebras)

class virtual ['repr] sym = object 
  method virtual lit : bool -> 'repr
  method virtual neg : 'repr -> 'repr
  method virtual an_ : 'repr -> 'repr -> 'repr
  method virtual or_ : 'repr -> 'repr -> 'repr
end

*)


(* Boring: all constants. 
   Need some inputs. So we extend the language
*)

module type SYMW = sig
  include SYM
  val wire_x : repr
  val wire_y : repr
  val wire_z : repr
end

type 'a symwmod = (module SYMW with type repr = 'a)


(* the alternative
class virtual ['repr] symw = object 
  inherit ['repr] sym
  method virtual wire_x : 'repr
  method virtual wire_y : 'repr
  method virtual wire_z : 'repr
end
*)

let ex2 : type a. a symwmod -> a = function (module I) -> let open I in
  lit true && neg wire_x

(* How to get xor? Extend the language again?
   This is like a _macro_ (we see later)
   We will also see a simpler way of writing the code below
*)
module XOR(I:SYM) = struct
  open I
  let xor x y = (x || y) && (neg (x && y))
end

(* Simple adder
 * (soon we could do just "let open XOR(I) in ...") 
*)

let exadd2 : type a. a symwmod -> a = function (module I) ->
  let module X = XOR(I) in let open X in let open I in
     xor wire_x wire_y

let exadd3 : type a. a symwmod -> a = function (module I) ->
  let module X = XOR(I) in let open X in let open I in
     xor (xor wire_x wire_y) wire_z

let exadd_xy1 : type a. a symwmod -> a = function (module I) ->
  let module X = XOR(I) in let open X in let open I in
    xor wire_x (xor wire_y (lit true))

(* So far, what we wrote are phrases in our DSL: well-formed phrases
   What do they mean?
*)



(* // *)

(* To understand what a phrase means we first have to figure out what
   words may mean. We need to implement the |SYM| signature.

An implementation, such as
|R| below, tells what |repr| is, concretely~-- 
and how exactly |lit|, |neg| etc. operations construct the
|repr| values that represent the boolean literal, the negation,
etc. terms.  In other words, an implementation of |SYM| tells how
to compute the meaning, or, the |repr| value, of each term from the
|repr| values of its immediate subterms. An
implementation of |SYM| thus specifies a denotational semantics
of the calculus (compositional, as it should be).
*)

(* Specifically, |R| maps SYM terms to OCaml booleans *)

module R = struct
  type repr = bool
  let lit x = x
  let neg = not
  let (&&) = Stdlib.(&&)
  let (||) = Stdlib.(||)
end

(* Recall,
module Ex1(I:SYM) = struct
  open I
  let wire = lit true && (neg (lit false))
end
*)

(* NNN
   Functor application
   It doesn't have to be local, but why pollute the namespace
*)
let _ = let module M=Ex1(R) in M.wire


(* Recall,
type 'a symmod = (module SYM with type repr = 'a)
let ex1 : type a. a symmod -> a = function (module I) -> let open I in
  lit true && (neg (lit false))
*)

(* NNN This is just passing to a function a record object *)
let _ = ex1(module R)

(* To run ex2, we need an instance of SYMW *)

(* This is a model of SYMW, with the interpretation function *)
module RW = struct
  include R
  let wire_x = true
  let wire_y = false
  let wire_z = true
end

(* What if we use a wrong interpreter

let _ = exadd2 (module R)
*)

let _ = exadd2 (module RW)



(* QUIZ
   The RW module defined one particular interpretation of wire_x,
   wire_y and wire_z
   What if we want a different interpretation?
   Discuss in OCaml or your favorite language
*)

(* This is one answer, in OCaml. Many answers exist, even in OCaml *)
let _ = exadd2 (module struct include RW let wire_x = false end)

 (* abstracting wire_x value *)
let exadd2_eval' x = exadd2 (module struct include RW let wire_x = x end)


(* // *)
(* Another interpreter, to show the circuit *)

(*





*)

(* Interpret |repr| as a string *)

module S0 = struct
  type repr = string

  let paren : string -> string = fun x -> "(" ^ x ^ ")"
 
  let lit x = if x then "tt" else "ff"
  let neg x   = "~" ^ x
  let (&&) x y = paren @@ x ^ " && " ^ y
  let (||) x y = paren @@ x ^ " || " ^ y
end

module SW0 = struct
  include S0
  let wire_x = "X"
  let wire_y = "Y"
  let wire_z = "Z"
end

let exadd2_view = exadd2 (module SW0)
let exadd3_view = exadd3 (module SW0)

(* Is it clear that xor was a macro? *)

(* QUIZ: How to avoid too many parenthesis?
   Write a precedence printer
*)

(*


























*)

(* Simplification is in order! *)

(* QUIZ *)
(* write an interpreter to draw the circuit as a diagram *)

(* QUIZ *)
(* write an interpreter to compute the latency (circuit switching delay) *)


