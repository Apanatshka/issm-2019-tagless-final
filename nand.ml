(* How to implement the circuit if we only have NAND gates? 
 * (show two ways, one using normalization)
 * Give the meaning of the circuit in terms of NAND gates.
 * Prelude to NBE (non-standard interpretation)
*)
(* 

*)
open Basic_gates

module type NAND = sig
  type repr

  val lit  : bool -> repr
  val nand : repr -> repr -> repr
end

module SN = struct
  type repr = string

  let paren : string -> string = fun x -> "(" ^ x ^ ")"
 
  let lit x = if x then "tt" else "ff"
  let nand x y = paren @@ x ^ " ^^ " ^ y
end

module TNAND(I:NAND) = struct
  type repr = I.repr

  let lit = I.lit
  let neg x   = I.nand x x
  let (&&) x y = neg (I.nand x y)
  (* neg (neg (or x y)) = neg (and (neg x) (neg y)) *)
  let (||) x y = I.nand (neg x) (neg y)
end

let _ = ex1 (module S0)

let _ = ex1 (module TNAND(SN))

(* Recall,
let exadd3 : type a. a symwmod -> a = function (module I) ->
  let module X = XOR(I) in let open X in let open I in
     xor (xor wire_x wire_y) wire_z
 *)

let exadd3 : type a. a symmod -> a*a*a -> a = fun (module I) (x,y,z) ->
  let module X = XOR(I) in let open X in let open I in
     xor (xor x y) z

let _ = exadd3(module TNAND(SN)) ("x","y","z")

(* Simplification is in order *)
(* QUIZ consider the NAND simplification *)

;;
