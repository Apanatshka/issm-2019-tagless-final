(* Tagless-final and Universal Algebra *)

(* Recall SYM *)
module type SYM = sig
  type repr

  val lit  : bool -> repr
  val neg  : repr -> repr
  val (&&) : repr -> repr -> repr
  val (||) : repr -> repr -> repr
end

(* This is an algebraic signature. What is Algebra? What is an algebraic
   signature?
   http://okmij.org/ftp/tagless-final/Algebra.html
*)

(* 




*)

module R = struct
  type repr = bool
  let lit x = x
  let neg = not
  let (&&) = Stdlib.(&&)
  let (||) = Stdlib.(||)
end

(* It is one of the algebras of the SYM signature. The carrier set is
   OCaml boolean values, and lit true, lit false, neg. etc. are the
   operations, *total functions* on these values.
*)

module S0 = struct
  type repr = string

  let paren : string -> string = fun x -> "(" ^ x ^ ")"
 
  let lit x = if x then "tt" else "ff"
  let neg x   = "~" ^ x
  let (&&) x y = paren @@ x ^ " && " ^ y
  let (||) x y = paren @@ x ^ " || " ^ y
end

(* This is another SYM algebra -- this time with an infinite carier set:
   set of all OCaml strings
*)


(* A SYM term like |neg (lit false)| means OCaml true value according to the
   R semantics, and the string "~ff" according to the S0 semantics.
   But what |neg (lit false)| means *by itself*?
*)






(* A term embedded in the tagless-final style means whatever 
   its interpretation makes it to mean. 

Technically, an embedded term may be regarded as a function that takes
an interpretation of its syntactic forms (i.e., an implementation of
its signature) and returns the |repr| value produced by that
implementation. To say it in OCaml:
*)

type sym = {e: 'a. (module SYM with type repr = 'a) -> 'a}

(*











*)

(* Although |sym| is the glib technical answer to the question about
the inherent meaning of embedded terms, it could be taken
seriously. In fact, we may define a denotational semantics in which
our lambda-calculus terms indeed mean |sym|. 
It is the most banal~-- one may say, `syntactic'~-- implementation
of |SYM|. We show only the representative part; the reader may want to finish as
an exercise.
*)

module SYMSelf = struct
  type repr = sym

  let lit : bool -> repr = fun x -> 
    {e = fun (type a) (module I : SYM with type repr=a) -> I.lit x}

  let neg : repr -> repr = fun {e=e} -> 
    {e = fun (type a) ((module I : SYM with type repr=a) as i) -> I.neg (e i)}








  let (&&) : repr -> repr -> repr = fun {e=e1} {e=e2}-> 
    {e = fun (type a) ((module I : SYM with type repr=a) as i) -> 
      I.((e1 i) && (e2 i))}

  let (||) : repr -> repr -> repr = fun {e=e1} {e=e2}-> 
    {e = fun (type a) ((module I : SYM with type repr=a) as i) -> 
      I.((e1 i) || (e2 i))}
end




(* Sample term *)
open SYMSelf

let ex0 = neg (lit false)


(* One can now say what |neg (lit false)| means `by itself': it tells how to
interpret itself, using whatever interpreter the user may provide.

That is, |neg (lit false)| takes an interpreter as an argument and directs it to
find the meanings of |lit false| and use the interpretation of |neg|
to build the complete denotation. 

The `intrinsic' meaning of an embedded term is, hence, the guiding 
of its interpretation according to its structure. 
This is what *syntax* means, doesn't it?
*)


(*





*)

(* Of all |SYM|-algebras, |SYMSelf| is special: it is so-called
`word' algebra, which is an initial algebra. For any
|SYM|-algebra |A|, there is a unique total function 
|SYMSelf.repr -> A.repr| that `respects' the operations: the unique
homomorphism. 
*)

(* Indeed, here it is *)
let h : type arep. (module SYM with type repr = arep) -> SYMSelf.repr -> arep = 
  fun moduleA self -> self.e moduleA

(* Let us verify that operations are `respected', 
  using S0 algebra as an example. 
*)
(*




*)
  



(* Notice, any
implementation of a |SYM|-signature is exactly the definition of 
that homomorphism. For example, passing the |S0| interpreter, as the
first-class--module argument, to a
|SYMSelf| term obtains the |S0|'s interpretation of the term
*)

let _ = h (module S0) ex0

let _ = ex0.e (module S0)

(*



*)
(* Earlier:
 * let ex1 =  type a. a symmod -> a = function (module I) -> let open I in
 *   lit true && (neg (lit false))
 *)
let ex1 = lit true && (neg (lit false)) (* earlier example *)

let view : sym -> string = fun {e} -> e (module S0)
let _ = view ex1

(* Conclusion: in the tagless-final style, an interpreter is an algebra
   defined by its homomorphism from the initial, word algebra
*)


(* SYM is rather boring, with only two constants lit false and lit true.
  We need more zero-arity symbols, whose interpretation is not so
  firmly baked-in. We need `propositional letters', which could be
  interpreted *freely*.
*)
(* Earlier we introduced the propositional letters like wire_x  by
   extending the signature, to remind:
*)
module type SYMW = sig
  include SYM
  val wire_x : repr
  val wire_y : repr
  val wire_z : repr
end

(* As before, we can build the SYMW-word algebra (SYMW-initial algebra)
   But can we introduce `variables' without changing the signature?
   Just remaining to be SYM algebra?
*)

(* Let's consider a few motivating examples *)
type 'a symmod = (module SYM with type repr = 'a)
let exx1 : type a. a symmod -> a -> a = function (module I) -> fun x ->
  let open I in
  lit true && (neg x)

let exx2 : type a. a symmod -> a*a -> a = function (module I) -> fun (x,y) ->
  let open I in
  (x || y) && (neg (x && y))

let _ = exx2 (module S0)
let _ = exx2 (module S0) ("x","y")

(* To generalize *)

(*





















































*)

(* If A is a SYM algebra with carrier A.repr and if 
   h : Env -> A.repr is any function mapping `variables' into A.repr,
   then there exists a unique homomorphism 
   h': SYMFree.repr(Env) -> A.repr extending h,
   that is, h'(SYMFree.x) = h(x) for any x in env
*)

(* 
(* 


 *)


(*
 * let ex2 : type a. a symwmod -> a = function (module I) -> let open I in
 *   lit true && neg wire_x
 *)








*)
