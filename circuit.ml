(* Circuits: gate functions *)


(* L 
#load "basic_gates.cmo";;
#load "bconst_prop.cmo";;
#load "cnf.cmo";;
*)

open Basic_gates
open Bconst_prop
open Cnf

(* Previously we had what was an `untyped' language of wires:
 To be sure, the OCaml embedding is certainly typed.
 However, the was no type associated with wires (because it was all
 the same). Each wire/gate assembly was represented as OCaml values
 of the same type |repr|, with no further statically visible distinction.

module type SYM = sig
  type repr

  val lit  : bool -> repr
  val neg  : repr -> repr
  val (&&) : repr -> repr -> repr
  val (||) : repr -> repr -> repr
end

*)

(* We have a reason to attach types: we want to distinguish just wires/gate
   assemblies from circuits, with defined input and output. Furthermore,
   we want to introduce `wire bundles' (busses) and circuits with
   several inputs and several outputs.
   There reason for introducing types is to statically reject patently
   stupid assemblies (e.g., dangling wires),
   and (potentially) help build helpful IDE/smart editors with completion,
   coloring etc.
*)

(* NNN First see CIRC, and then return to RecordType *)

(* Semi-dynamic record type. We shall soon see why we need it and why
   it is not fully dynamic.

   Discuss the spectrum of static-dynamic typing!
 *)
module RecordType : sig
  type 'a desc
  val fields : 'a desc -> string list   (* sorted *)
  val show   : 'a desc -> string
  module Make : 
    functor(F: sig val field_names : string list end) ->
    sig type t val t : t desc end
 end = struct
   type 'a desc = string list
   let fields x = x
   let show x = "[" ^ String.concat ";" x ^ "]"
   module Make(F: sig val field_names : string list end) = struct
     type t
     let t = List.sort compare (F.field_names)
     (* check for no duplicates *)
     let () = assert List.(t = sort_uniq compare F.field_names)
   end
end

open RecordType

(* To gates, we add ports and circuits. A port is like a bus: a bundle
   of wires. 
*)
module type PORT = sig
  type repr
  type 'a port
  val ( .%{} ) : 'a port -> string -> repr
  val port : 'a desc -> (string * repr) list -> 'a port
  val cast : 'a desc -> 'b port -> 'a port
end

(*


 *)
module type CIRC = sig
  include SYM
  include PORT with type repr := repr

  type 'a circ
  val lam : 'a desc -> ('a port -> 'b port) -> ('a -> 'b) circ
  val (//) : ('a -> 'b) circ -> ('a port -> 'b port) (* `application', infix *)
end

(* NNN It's better to see examples first, and then discuss a few details
   and variations of the implementation. Draw the circuits!
 *)
(* Adders *)

(* Declare a few `record types'
   They seem quite general, and so declared `at the top level'
 *)
module Pxy = RecordType.Make(struct let field_names=["x";"y"] end)
module Pad = RecordType.Make(struct let field_names=["sum";"carry"] end)

module HalfAdder(I:CIRC) = struct
  open I
  include XOR(I)

  let ha = lam Pxy.t @@ fun pin ->
    let sum = xor pin.%{"x"} pin.%{"y"} in
    let cout = pin.%{"x"} && pin.%{"y"} in
    port Pad.t [("sum",sum);("carry",cout)]
end

module FullAdder(I:CIRC) = struct
  open I
  module HAdder = HalfAdder(I)
  module Pxycin = RecordType.Make(struct let field_names=["x";"y";"cin"] end)

  let fa = lam Pxycin.t @@ fun pin ->
                           (* what happens if we omit the cast *)
    let xy  = HAdder.ha // cast Pxy.t pin in
    let xyc = HAdder.ha // port Pxy.t [("x",xy.%{"sum"}); ("y",pin.%{"cin"})] in
    let cout = xy.%{"carry"} || xyc.%{"carry"} in (* can't be both 1 *)
    port Pad.t [("sum",xyc.%{"sum"});("carry",cout)]
end

(* Try to omit the cast: this is not totally dynamically typed
  The notation is not totally off-base:
  See the ForSyDe thesis, PDF page 34 (and then the discussion of the drawback
  of pairs) and (PDF page 55) for port connections
*)

module PORTBase(S:sig type repr end) = struct
  open S
  type 'a port = (string * repr) list

  let brackets x = "[" ^ x ^ "]"
  let show_port_type : 'a port -> string = fun p ->
    List.map fst p |> String.concat ";" |> brackets

  let ( .%{} ) : 'a port -> string -> repr = fun p key -> 
    try List.assoc key p             (* also does a `type check' *)
    with Not_found -> 
      failwith @@ "No key " ^ key ^ " in port " ^ show_port_type p
 
  let port : 'a desc -> (string * repr) list -> 'a port = fun d l ->
    assert List.(fields d = sort compare (map fst l)); (* `type check' *)
    l

  let cast : 'a desc -> 'b port -> 'a port = fun d p ->
    fields d |> List.map (fun f -> (f,p.%{f})) 
end

module RC(I:SYM) = struct
  include I
  include PORTBase(I)

  type _ circ = C : ('a port -> 'b port) -> ('a -> 'b) circ 
  let lam : 'a desc -> ('a port -> 'b port) -> ('a -> 'b) circ = fun _ f ->
    C f

  let (//) : ('a -> 'b) circ -> ('a port -> 'b port) = fun (C f) p -> f p

end


let _ = 
  let module RR = RC(R) in
  let module M = FullAdder(RR) in
  let open RR in
  M.fa // port M.Pxycin.t [("x",lit true);("y",lit false);("cin",lit true)]

(* Pretty-printing AND a record type check *)

(* Pretty-printing seems straightforward at first *)

module SC0(I:SYM with type repr = string) = struct
  include I
  include PORTBase(I)

  let counter = ref 0
  let genint : unit -> int = fun () -> incr counter; !counter
  let reset : unit -> unit = fun () -> counter := 0

  let gensym : string -> string = fun prefix ->
    prefix ^ string_of_int (genint ())

  let show_port : 'a port -> string = fun d ->
    List.map (fun (f,e) -> f ^ ":" ^ e) d |> String.concat ";" |> brackets

  type 'a circ = string * string list
  let lam : 'a desc -> ('a port -> 'b port) -> ('a -> 'b) circ = fun d f ->
    let piname = gensym "pi" in
    let pi = fields d |> List.map (fun f -> (f,piname ^ "." ^ f)) in
    let pout = f pi in
    let cname = "{" ^ piname ^ "@" ^ RecordType.show d ^
                  " -> \n" ^ show_port pout ^ "}" in
    (cname, List.map fst pout)

  let (//) : ('a -> 'b) circ -> ('a port -> 'b port) = fun (cn,cout) p ->
    (* no need to record type check. Why? *)
    let pn = (cn ^ " // " ^ show_port p) in
    List.map (fun f -> (f,pn ^ "." ^ f)) cout
end

let _ = 
  let module M = HalfAdder(SC0(S0)) in
  print_endline @@ fst @@ M.ha
(* So far so good *)

let _ = 
  let module I = SC0(S0) in
  let module M = HalfAdder(I) in
  print_endline @@
  I.(show_port @@ M.ha // port Pxy.t [("x",lit true);("y",lit true)])

(* See the problem? *)

let _ = 
  let module M = FullAdder(SC0(S0)) in
  print_endline @@ (fst M.fa)
(* It gets worse *)
;;

(* Why this is not entirely straightforward: See the FullAdder,
   draw its circuit diagram and note that port xy is connected two
   to different other ports (although via different pins)
 *)
module SC(I:SYM with type repr = string) = struct
  include I
  include PORTBase(I)

  let counter = ref 0
  let genint : unit -> int = fun () -> incr counter; !counter
  let reset : unit -> unit = fun () -> counter := 0

  let gensym : string -> string = fun prefix ->
    prefix ^ string_of_int (genint ())

  let toinsert = ref ""
  let genlet : string -> string -> string = fun prefix e ->
    let name = gensym prefix in
    toinsert := !toinsert ^ "let " ^ name ^ " = " ^ e ^ " in\n";
    name
  let inserted : unit -> string = fun () ->
    let r = !toinsert in
    toinsert := ""; r

  (* Override of the one in PORTBase *)
  let port : 'a desc -> (string * repr) list -> 'a port = fun d l ->
    (* Does the record `type' check. Error reporting could be better *)    
    assert List.(fields d = sort compare (map fst l));
    List.map (fun (f,e) -> (f,genlet f e)) l

  let show_port : 'a port -> string = fun d ->
    List.map (fun (f,e) -> f ^ ":" ^ e) d |> String.concat ";" |> brackets

  type 'a circ = string * string list
  let lam : 'a desc -> ('a port -> 'b port) -> ('a -> 'b) circ = fun d f ->
    let piname = gensym "pi" in
    let pi = fields d |> List.map (fun f -> (f,piname ^ "." ^ f)) in
    let () = assert (!toinsert = "") in (* sanity check *)
    let pout = f pi in
    let lets = inserted () in
    let cname = "{" ^ piname ^ "@" ^ RecordType.show d ^
                  " -> \n" ^ lets ^ show_port pout ^ "}" in
    (cname, List.map fst pout)

  let (//) : ('a -> 'b) circ -> ('a port -> 'b port) = fun (cn,cout) p ->
    (* no need to record type check. Why? *)
    let pn = genlet "po" (cn ^ " // " ^ show_port p) in
    List.map (fun f -> (f,pn ^ "." ^ f)) cout

  let observe_port : 'a port -> string = fun p ->
    inserted () ^ show_port p
end

(* QUIZ Check the circuit for closedness? *)

let _ = 
  let module M = HalfAdder(SC(S0)) in
  print_endline @@ fst @@ M.ha

let _ = 
  let module I = SC(S0) in
  let module M = HalfAdder(I) in
  print_endline @@
  I.(observe_port @@ M.ha // port Pxy.t [("x",lit true);("y",lit true)])

let _ = 
  let module M = FullAdder(SC(S0)) in
  print_endline @@ (fst M.fa)

;;

let _ = 
  let module I = SC(S0) in
  let module M = FullAdder(I) in
  let open I in
  print_endline @@ observe_port @@
  M.fa // port M.Pxycin.t [("x",lit true);("y",lit false);("cin",lit true)]

(* QUIZ 
   Implement circuits with not just bool wire but int32 wire (thick wire),
   and, in general, wire of a given width
*)

(* The code is imperative. Listen to Jeremy's talk at the ML workshop
   in Berlin about a `purely mathematical' version
 *)

(* QUIZ 
   Alternative design:
   In CIRC, lam takes the descriptor of the input bus: think of it as
   the `type annotation' in simply-type lambda calculus.
   We could easily infer it. When we show the circuit, we can also
   print the (inferred) width of all the busses.
   (We chose the explicit annotations because it is a bit easier
   to explain, and also in deference to VHDL/Verilog where
   widths are explicitly declared.)
*)


(*
 * In the earlier SYM, SYM.repr is now bool wire CIRC.repr.
 * (thus we could use unityped CIRC.repr as an implementation for the
 * earlier untyped SYM)
 *
*)

(*


















*)

(* XXX Make a good example to show constant-propagation ... *)
let _ = 
  let module M = HalfAdder(SC(CP(SWO0))) in
  M.ha

(* Circuit-specific transformations *)

(* Naive inlining *)
module InlineNaiveC(I:CIRC) = struct
  include ID(I)
  type 'a port = 'a I.port
  let ( .%{} ) = I.( .%{} )
  let port = I.port
  let cast = I.cast

  type _ circ = C : 'a desc * ('a port -> 'b port) -> ('a->'b) circ
  let lam d f = C (d,f)
  let (//) (C (_,f)) p = f p

  let observe_circ : 'a circ -> 'a I.circ = fun (C (d,f)) -> I.lam d f
end

let _ = 
  let module I = SC(S0) in
  let module M = HalfAdder(I) in
  let open I in
  print_endline @@ observe_port @@
  M.ha // port Pxy.t [("x",lit true);("y",lit false)]

(* XXX *)
let _ = 
  let module I = InlineNaiveC(SC(S0)) in
  let module M = HalfAdder(I) in
  let module S'' = SC(S0) in
  let open I in
  print_endline @@ S''.observe_port @@
  M.ha // port Pxy.t [("x",lit true);("y",lit false)]


let _ = 
  let module I = InlineNaiveC(SC(S0)) in
  let module M = FullAdder(I) in
  print_endline @@ (fst (I.observe_circ M.fa))

;;

(* Recall *)
let _ = 
  let module I = SC(S0) in
  let module M = FullAdder(I) in
  let open I in
  print_endline @@ observe_port @@
  M.fa // port M.Pxycin.t [("x",lit true);("y",lit false);("cin",lit true)]

let _ = 
  let module I = SC(S0) in
  let module I' = InlineNaiveC(I) in
  let module M = FullAdder(I') in
  let open I' in
  print_endline @@ I.observe_port @@
  M.fa // port M.Pxycin.t [("x",lit true);("y",lit false);("cin",lit true)]
*)

(*














 * to True. We should enhance our PE to do known applications as well.
 *)

(* HO optimizations: applying a statically-known function *)

module DStaticApp(F:SYMHO) = struct
  module X = struct
    type 'a from = 'a F.repr
          (* This is not a recursive type ! *)
    type 'a term = 
      | Unk : 'a from -> 'a term
            (* statically-known function *)
      | Fun : ('a term -> 'b term) -> ('a -> 'b) term
    let fwd x = Unk x
    let rec bwd : type a. a term -> a from = function
      | Unk x -> x
      | Fun f -> F.lam (fun x -> bwd (f (fwd x)))
  end
  include X
  include Trans_def(X)
  module IDelta = struct
   let lam f = Fun f
   let app: ('a->'b) term -> 'a term -> 'b term = fun f x ->
   match f with
   | Fun ff -> ff x
   | _      -> Unk (F.app (bwd f) (bwd x))
 end
end

module PStaticApp(F:SYMHO) = struct
  module OptM  = DStaticApp(F)
  include SYMTHO(OptM)(F)
  include OptM.IDelta
end

let appHO = fun (type obs) e ((module F):obs symho) -> 
  e ((module PStaticApp(F)): obs symho)

let _ = viewHO @@ appHO @@ (ntimes 5 chainHO ehadd3t)

let _ = viewHO @@ ntimes 5 chainHO @@ appHO @@ (ntimes 5 chainHO ehadd3t)
;;

*)
