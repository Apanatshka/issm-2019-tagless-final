#![allow(dead_code)]

use crate::basic_gates;
use std::marker::PhantomData;

/* Box boxes a type, i.e. it is allocated on the heap. This kind of indirection is necessary
   for an recursive datatype. Rust normally uses puts data inline, and will fail to do so for
   the recursion, noting that the size of the type would be infinite.
*/
pub enum Symdata {
  Lit(bool),
  WireX,
  Neg(Box<Symdata>),
  And(Box<Symdata>, Box<Symdata>),
  Or(Box<Symdata>, Box<Symdata>),
}

/* These are convenience functions to construct the cases that need to box their children. I
   don't think it is idiomatic Rust to hide this kind of allocation cost. But it's convenient
   in `neg_down`.
*/
fn neg(x: Symdata) -> Symdata {
  Symdata::Neg(Box::new(x))
}

fn and(x: Symdata, y: Symdata) -> Symdata {
  Symdata::And(Box::new(x), Box::new(y))
}

fn or(x: Symdata, y: Symdata) -> Symdata {
  Symdata::Or(Box::new(x), Box::new(y))
}

pub fn exd() -> Symdata {
  use Symdata::*;
  neg(and(Lit(true), neg(WireX)))
}

/* NOTE: The box keyword in the patterns are a language extension in Rust.
         Only the nightly version of the compiler accepts such language extensions.
*/
fn neg_down(input: Symdata) -> Symdata {
  use Symdata::*;
  match input {
    Neg(box Lit(true)) => neg(Lit(false)),
    Neg(box Lit(false)) => neg(Lit(true)),
    Neg(box Neg(box x)) => neg_down(x),

    Neg(box And(x, y)) => or(neg_down(Neg(x)), neg_down(Neg(y))),
    Neg(box Or(x, y)) => and(neg_down(Neg(x)), neg_down(Neg(y))),

    And(box x, box y) => and(neg_down(x), neg_down(y)),
    Or(box x, box y) => or(neg_down(x), neg_down(y)),
    e => e,
  }
}

pub fn ignore1() -> Symdata {
  neg_down(exd())
}

#[derive(Eq, PartialEq, Copy, Clone)]
pub enum Ctx {
  Pos,
  Neg,
}

struct TNegDown<I> {
  pd: PhantomData<I>
}
impl<I: basic_gates::Sym> basic_gates::Sym for TNegDown<I> where I::Repr: 'static {
  type Repr = Box<dyn Fn(Ctx) -> I::Repr>;

  fn lit(x: bool) -> Self::Repr {
    Box::new(move |ctx: Ctx| {
      match ctx {
        Ctx::Pos => I::lit(x),
        Ctx::Neg => I::lit(!x),
      }
    })
  }

  fn neg(e: Self::Repr) -> Self::Repr {
    Box::new(move |ctx: Ctx| {
      match ctx {
        Ctx::Pos => e(Ctx::Neg),
        Ctx::Neg => e(Ctx::Pos),
      }
    })
  }

  fn and(e1: Self::Repr, e2: Self::Repr) -> Self::Repr {
    Box::new(move |ctx: Ctx| {
      match ctx {
        Ctx::Pos => I::and(e1(Ctx::Pos), e2(Ctx::Pos)),
        Ctx::Neg => I::or(e1(Ctx::Neg), e2(Ctx::Neg)),
      }
    })
  }

  fn or(e1: Self::Repr, e2: Self::Repr) -> Self::Repr {
    Box::new(move |ctx: Ctx| {
      match ctx {
        Ctx::Pos => I::or(e1(Ctx::Pos), e2(Ctx::Pos)),
        Ctx::Neg => I::and(e1(Ctx::Neg), e2(Ctx::Neg)),
      }
    })
  }
}
