#![allow(dead_code)]

use crate::basic_gates;

use std::rc::Rc;
use std::marker::PhantomData;

pub trait NAnd {
  type Repr;

  fn lit(x: bool) -> Self::Repr;
  fn nand(x: Self::Repr, y: Self::Repr) -> Self::Repr;
}

pub struct SN;
impl NAnd for SN {
  type Repr = Rc<String>;

  fn lit(x: bool) -> Self::Repr {
    if x {
      Rc::new("tt".to_string())
    } else {
      Rc::new("ff".to_string())
    }
  }
  fn nand(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    Rc::new(basic_gates::paren(format!("{} ^^ {}", x, y)))
  }
}

struct TnAnd<I> {
  pd: PhantomData<I>,
}
impl<I: NAnd> basic_gates::Sym for TnAnd<I> where I::Repr: Clone {
  type Repr = I::Repr;

  fn lit(x: bool) -> Self::Repr {
    I::lit(x)
  }
  fn neg(x: Self::Repr) -> Self::Repr {
    I::nand(x.clone(), x)
  }
  fn and(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    Self::neg(I::nand(x, y))
  }
  /* neg (neg (or x y)) = neg (and (neg x) (neg y)) */
  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    I::nand(Self::neg(x), Self::neg(y))
  }
}

pub fn ignore1() -> Rc<String> {
  basic_gates::ex1::<basic_gates::S0>()
}

pub fn ignore2() -> Rc<String> {
  basic_gates::ex1::<TnAnd<SN>>()
}