#![allow(dead_code)]

use std::rc::Rc;
use std::marker::PhantomData;

/*
Basic gates and circuits

The tagless-final style is designed around a compositional mapping of
terms of the embedded DSL to the values in some |repr| domain -- i.e.,
denotational semantics.

 * Define the syntax of circuits (in the form close to BNF)
 * Read  ``repr'' as a wire
*/

pub trait Sym {
  type Repr;

  fn lit(x: bool) -> Self::Repr;
  fn neg(x: Self::Repr) -> Self::Repr;
  fn and(x: Self::Repr, y: Self::Repr) -> Self::Repr;
  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr;
}

/* NNN
Module, aka struct: a collection of type declaration and definitions
Module type, aka signature: a collection of type declarations and types of
the defined symbols
Modules have a lot in common with records or objects (sans declarations),
or namespaces
and module types -- with virtual/abstract classes
*/


/* BNF view
The signature |SYM| can be read as the context-free grammar of the language:
|repr| is the start symbol and |lit|, |neg|, etc. are the
terminals. In the conventional CFG notation it would look as:

repr -> "lit" "true" | "lit" "false"
repr -> "neg" repr
repr -> repr "&&" repr
repr -> repr "||" repr

This grammar derives, for example, |lit true && lit false|.
*/

/* OCaml programmer's view

*/

/* Example
NNN module, parameterized module (functor), open, let definition
*/
pub fn ex1<I: Sym>() -> I::Repr {
  I::and(I::lit(true), I::neg(I::lit(false)))
}

/* Boring: all constants.
Need some inputs. So we extend the language
*/

pub trait SymW : Sym {
  fn wire_x() -> Self::Repr;
  fn wire_y() -> Self::Repr;
  fn wire_z() -> Self::Repr;
}

pub fn ex2<I: SymW>() -> I::Repr {
  I::and(I::lit(true), I::neg(I::wire_x()))
}

pub struct Xor<I> {
  pd: PhantomData<I>,
}

impl<I: Sym> Xor<I> where I::Repr: Clone {
  fn xor(x: I::Repr, y: I::Repr) -> I::Repr {
    I::and(I::or(x.clone(), y.clone()), I::neg(I::and(x, y)))
  }
}

/* NOTE: A possibly more Rustic implementation here would be the default method implementation,
         as seen in the following module. It
*/
mod alternative_xor {
  use super::{Sym, SymW};

  pub trait Xor : Sym where Self::Repr: Clone {
    fn xor(x: Self::Repr, y: Self::Repr) -> Self::Repr  {
      Self::and(Self::or(x.clone(), y.clone()),  Self::neg(Self::and(x, y)))
    }
  }

  pub fn exadd2<I: Xor + SymW>() -> I::Repr where I::Repr: Clone {
    I::xor(I::wire_x(), I::wire_y())
  }
}

/* Simple adder
*/

pub fn exadd2<I: SymW>() -> I::Repr where I::Repr: Clone {
  Xor::<I>::xor(I::wire_x(), I::wire_y())
}

pub fn exadd3<I: SymW>() -> I::Repr where I::Repr: Clone {
  Xor::<I>::xor(Xor::<I>::xor(I::wire_x(), I::wire_y()), I::wire_z())
}

pub fn exadd_xy1<I: SymW>() -> I::Repr where I::Repr: Clone {
  Xor::<I>::xor(I::wire_x(), Xor::<I>::xor(I::wire_y(), I::lit(true)))
}

/* So far, what we wrote are phrases in our DSL: well-formed phrases
What do they mean?
*/

/* To understand what a phrase means we first have to figure out what
words may mean. We need to implement the |SYM| signature.

An implementation, such as
|R| below, tells what |repr| is, concretely~--
and how exactly |lit|, |neg| etc. operations construct the
|repr| values that represent the boolean literal, the negation,
etc. terms.  In other words, an implementation of |SYM| tells how
to compute the meaning, or, the |repr| value, of each term from the
|repr| values of its immediate subterms. An
implementation of |SYM| thus specifies a denotational semantics
of the calculus (compositional, as it should be).
*/

/* Specifically, |R| maps SYM terms to OCaml booleans */
pub struct R {}
impl Sym for R {
  type Repr = bool;

  fn lit(x: bool) -> Self::Repr {
    x
  }

  fn neg(x: Self::Repr) -> Self::Repr {
    !x
  }

  fn and(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    x && y
  }

  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    x || y
  }
}

/* NNN
Functor application

ex1(R)
*/

fn ignore1() -> bool {
  ex1::<R>()
}

/* This is a model of SymW, with the interpretation function */
impl SymW for R {
  fn wire_x() -> Self::Repr {
    true
  }
  fn wire_y() -> Self::Repr {
    false
  }
  fn wire_z() -> Self::Repr {
    true
  }
}

/* What if we use a wrong interpreter

let _ = exadd2 (module R)
*/

fn ignore2() -> bool {
  exadd2::<R>()
}

/* QUIZ
The R struct defined one particular interpretation of wire_x,
wire_y and wire_z
What if we want a different interpretation?
Discuss in OCaml or your favorite language
*/

// Err.. I don't think this is elegantly possible in Rust.

/* Another interpreter, to show the circuit:
Interpret |repr| as a string */

pub fn paren(x: String) -> String {
  format!("({})", x)
}

pub struct S0;
impl Sym for S0 {
  type Repr = Rc<String>;

  fn lit(x: bool) -> Self::Repr {
    if x {
      Rc::new("tt".to_string())
    } else {
      Rc::new("ff".to_string())
    }
  }

  fn neg(x: Self::Repr) -> Self::Repr {
    Rc::new(format!("~{}", *x))
  }

  fn and(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    Rc::new(paren(format!("{} && {}", x, y)))
  }

  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    Rc::new(paren(format!("{} || {}", x, y)))
  }
}

impl SymW for S0 {
  fn wire_x() -> Self::Repr {
    Rc::new("X".to_string())
  }
  fn wire_y() -> Self::Repr {
    Rc::new("Y".to_string())
  }
  fn wire_z() -> Self::Repr {
    Rc::new("Z".to_string())
  }
}

pub fn exadd2_view() -> Rc<String> {
  exadd2::<S0>()
}

mod alternative_xor_usage {
  use std::rc::Rc;
  use super::alternative_xor::*;
  use super::S0;

  // trivial impl, method already has a default implementation
  impl Xor for S0 {}

  pub fn exadd2_view() -> Rc<String> {
    exadd2::<S0>()
  }
}

/* Is it clear that xor was a macro? */

/* Simplification is in order! */

/* QUIZ
write an interpreter to draw the circuit as a diagram */

/* QUIZ
write an interpreter to compute the latency (circuit switching delay) */

