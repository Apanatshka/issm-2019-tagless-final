#![allow(dead_code)]

use crate::basic_gates;
use crate::basic_gates::{Sym, SymW, S0};
use crate::bconst_prop::{SymO, InjPrj, Cp, CpRepr};

use std::rc::Rc;
use std::marker::PhantomData;

pub fn cart<A, B, C, F: FnMut((A,B)) -> C>(f: F, xs: Vec<A>, ys: Vec<B>) -> Vec<C> {
  xs.into_iter().zip(ys.into_iter()).map(f).collect()
}

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Polarity {
  Pos,
  Neg,
}
#[derive(Eq, PartialEq, Clone)]
pub struct Atom<R>(Polarity, R);
pub type Claw<R> = Vec<Atom<R>>;
pub type CnfRepr<R> = Vec<Claw<R>>;

pub struct Cnf<I> {
  pd: PhantomData<I>,
}
impl<I: Sym> InjPrj<I::Repr, CnfRepr<I::Repr>> for Cnf<I> {
  fn inj(x: I::Repr) -> CnfRepr<I::Repr> {
    vec![vec![Atom(Polarity::Pos,x)]]
  }
  fn prj(x: CnfRepr<I::Repr>) -> I::Repr {
    let build_lit = |a: Atom<I::Repr>| -> I::Repr {
      match a {
        Atom(Polarity::Pos, x) => x,
        Atom(Polarity::Neg, x) => I::neg(x),
      }
    };
    let build_disj = |l: Claw<I::Repr>| -> I::Repr {
      l.into_iter().map(build_lit).fold(I::lit(false), I::or)
    };
    let build_conj = |l: CnfRepr<I::Repr>| -> I::Repr {
      l.into_iter().map(build_disj).fold(I::lit(true), I::and)
    };
    build_conj(x)
  }
}
impl<I: Sym> Sym for Cnf<I> {
  type Repr = CnfRepr<I::Repr>;

  fn lit(x: bool) -> Self::Repr {
    Self::inj(I::lit(x))
  }

  fn and(mut x: Self::Repr, y: Self::Repr) -> Self::Repr {
    x.extend(y);
    x
  }

  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    cart(|(mut a,b)| {a.extend(b); a}, x, y)
  }

  fn neg(cs: Self::Repr) -> Self::Repr {
    let neg_atom = |a: Atom<I::Repr>| -> Atom<I::Repr> {
      match a {
        Atom(Polarity::Pos, x) => Atom(Polarity::Neg, x),
        Atom(Polarity::Neg, x) => Atom(Polarity::Pos, x),
      }
    };
    let neg_claw = |l: Claw<I::Repr>| -> Self::Repr {
      l.into_iter().map(|x| vec![neg_atom(x)]).collect()
    };
    cs.into_iter().map(neg_claw).fold(vec![vec![]], Self::or)
  }
}
impl<I: SymO> SymO for Cnf<I> {
  type Obs = I::Obs;

  fn observe(e: Self::Repr) -> Self::Obs {
    I::observe(Self::prj(e))
  }
}

pub fn ignore1() -> Rc<String> {
  basic_gates::ex1::<S0>()
}
pub fn ignore2() -> CnfRepr<Rc<String>> {
  basic_gates::ex1::<Cnf<S0>>()
}

impl<I: SymW> SymW for Cnf<I> {
  fn wire_x() -> Self::Repr {
    Self::inj(I::wire_x())
  }
  fn wire_y() -> Self::Repr {
    Self::inj(I::wire_y())
  }
  fn wire_z() -> Self::Repr {
    Self::inj(I::wire_z())
  }
}

pub fn ignore3() -> Rc<String> {
  basic_gates::ex2::<S0>()
}
pub fn ignore4() -> CnfRepr<Rc<String>> {
  basic_gates::ex2::<Cnf<S0>>()
}

pub fn wex1<I: SymW>() -> I::Repr {
  I::or(I::and(I::wire_x(), I::wire_y()), I::wire_z())
}

pub fn ignore5() -> Rc<String> {
  wex1::<S0>()
}
pub fn ignore6() -> Rc<String> {
  Cnf::<S0>::observe(wex1::<Cnf<S0>>())
}

pub fn ignore7() -> Rc<String> {
  basic_gates::exadd2::<S0>()
}
pub fn ignore8() -> CnfRepr<Rc<String>> {
  basic_gates::exadd2::<Cnf<S0>>()
}
pub fn ignore9() -> Rc<String> {
  Cnf::<S0>::observe(basic_gates::exadd2::<Cnf<S0>>())
}

pub fn ignore10() -> Rc<String> {
  basic_gates::exadd3::<S0>()
}
pub fn ignore11() -> Rc<String> {
  Cnf::<S0>::observe(basic_gates::exadd3::<Cnf<S0>>())
}

pub fn ignore12() -> Rc<String> {
  basic_gates::exadd_xy1::<S0>()
}
pub fn ignore13() -> Rc<String> {
  Cnf::<S0>::observe(basic_gates::exadd_xy1::<Cnf<S0>>())
}
pub fn ignore14() -> Rc<String> {
  Cnf::<Cp<S0>>::observe(basic_gates::exadd_xy1::<Cnf<Cp<S0>>>())
}
pub fn ignore15() -> CpRepr<CnfRepr<Rc<String>>> {
  basic_gates::exadd2::<Cp<Cnf<S0>>>()
}
pub fn ignore16() -> Rc<String> {
  Cp::<Cnf<S0>>::observe(basic_gates::exadd_xy1::<Cp<Cnf<S0>>>())
}