/* This feature is used in bneg_adhoc for convenience. Comment it and the use of bneg_adhoc in
   this file is you don't want to bother installing a nightly version of the compiler. The
   normal ("stable") version of the compiler refuses to handle these feature flags.
*/
#![feature(box_patterns)]

mod basic_gates;
mod nand;
mod bneg_adhoc;
mod bconst_prop;
mod cnf;
//mod circuit;

fn main() {
  println!("{}", cnf::ignore14());
  println!("{}", cnf::ignore16());
}
