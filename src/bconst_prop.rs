#![allow(dead_code)]

use crate::basic_gates::{Sym, SymW, S0};
use crate::basic_gates;
use std::marker::PhantomData;
use std::rc::Rc;

pub trait SymO : Sym {
  type Obs;

  fn observe(r: Self::Repr) -> Self::Obs;
}

pub struct Id<I> {
  pd: PhantomData<I>,
}
impl<I: Sym> Sym for Id<I> {
  type Repr = I::Repr;

  fn lit(x: bool) -> Self::Repr {
    I::lit(x)
  }
  fn neg(x: Self::Repr) -> Self::Repr {
    I::neg(x)
  }
  fn and(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    I::and(x, y)
  }
  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    I::or(x, y)
  }
}
impl<I: SymW> SymW for Id<I> {
  fn wire_x() -> Self::Repr {
    I::wire_x()
  }
  fn wire_y() -> Self::Repr {
    I::wire_y()
  }
  fn wire_z() -> Self::Repr {
    I::wire_z()
  }
}
impl<I: SymO> SymO for Id<I> {
  type Obs = I::Obs;

  fn observe(r: Self::Repr) -> Self::Obs {
    I::observe(r)
  }
}

pub trait InjPrj<Injectable, Projectable> {
  fn inj(x: Injectable) -> Projectable;
  fn prj(x: Projectable) -> Injectable;
}

impl<I: Sym> InjPrj<I::Repr, I::Repr> for Id<I> {
  fn inj(x: I::Repr) -> I::Repr {
    x
  }
  fn prj(x: I::Repr) -> I::Repr {
    x
  }
}

#[derive(Eq, PartialEq, Clone)]
pub enum CpRepr<R> {
  Lit(bool),
  Unk(R),
}

pub struct Cp<I> {
  pd: PhantomData<I>,
}

impl<I: Sym> Sym for Cp<I> {
  type Repr = CpRepr<I::Repr>;

  fn lit(x: bool) -> Self::Repr {
    CpRepr::Lit(x)
  }
  fn neg(x: Self::Repr) -> Self::Repr {
    use CpRepr::*;
    match x {
      Lit(x) => Lit(!x),
      Unk(e) => Unk(I::neg(e))
    }
  }
  fn and(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    use CpRepr::*;
    match (x,y) {
      (Lit(true), e) | (e, Lit(true)) => e,
      (Lit(false), _) | (_, Lit(false)) => Lit(false),
      (Unk(e1), Unk(e2)) => Unk(I::and(e1, e2)),
    }
  }
  fn or(x: Self::Repr, y: Self::Repr) -> Self::Repr {
    use CpRepr::*;
    match (x,y) {
      (Lit(true), _) | (_, Lit(true)) => Lit(true),
      (Lit(false), e) | (e, Lit(false)) => e,
      (Unk(e1), Unk(e2)) => Unk(I::or(e1, e2)),
    }
  }
}
impl<I: Sym> InjPrj<I::Repr, CpRepr<I::Repr>> for Cp<I> {
  fn inj(x: I::Repr) -> CpRepr<I::Repr> {
    CpRepr::Unk(x)
  }
  fn prj(x: CpRepr<I::Repr>) -> I::Repr {
    use CpRepr::*;
    match x {
      Unk(x) => x,
      Lit(x) => I::lit(x),
    }
  }
}
impl<I: SymO> SymO for Cp<I> {
  type Obs = I::Obs;

  fn observe(e: Self::Repr) -> Self::Obs {
    I::observe(Self::prj(e))
  }
}

impl<I: SymW> SymW for Cp<I> {
  fn wire_x() -> Self::Repr {
    Self::inj(I::wire_x())
  }
  fn wire_y() -> Self::Repr {
    Self::inj(I::wire_y())
  }
  fn wire_z() -> Self::Repr {
    Self::inj(I::wire_z())
  }
}

impl SymO for S0 {
  type Obs = Rc<String>;

  fn observe(x: Rc<String>) -> Rc<String> {
    x
  }
}

pub fn ignore1() -> Rc<String> {
  S0::observe(basic_gates::exadd_xy1::<S0>())
}

pub fn ignore2() -> Rc<String> {
  Cp::<S0>::observe(basic_gates::exadd_xy1::<Cp<S0>>())
}