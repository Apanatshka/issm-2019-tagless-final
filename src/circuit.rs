#![allow(dead_code)]

use crate::basic_gates;
use crate::basic_gates::{Sym, SymW, S0};
use crate::bconst_prop::{SymO, InjPrj, Cp, CpRepr};
use crate::cnf::{cart, Cnf, CnfRepr};

use std::rc::Rc;
use std::marker::PhantomData;

trait Port {
  type Port;
}

//type Desc<A> = Vec<Rc<String>>;
//fn fields<A>(x: Desc<A>) -> Vec<Rc<String>> {
//  x
//}
//fn show<A>(x: Desc<A>) -> Rc<String> {
//  Rc::new(format!("[{}]", x.into_iter().fold("", |a,b| format!("{};{}", a, b))))
//}
//
//trait APort<T> {}
//
//trait Port<R> {
//  fn named_wire<A, P: APort<A>>(port: P, name: &str) -> R;
//  fn port<A>
//}
